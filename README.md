# RunCl

Generator, Validator, Formatter/deformatter, for Chilean RUN (Rol Único Nacional)

## Installation

Add this line to your application's Gemfile:

    gem 'run_cl'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install run_cl

## Usage

At your model:

    include RunCl::ActAsRun
    validates  :rut, presence: true, uniqueness: true
    has_run_cl :rut

    Where 'rut' is the name of the column that stores the RUN.

If you are using factorygirl put this in your factory:

    FactoryGirl.define do
      factory :user do
        sequence(:rut) { Run.for(:user, :rut) }
        ....
      end
    end

    Where 'rut' is the name of the column that stores the RUN.


If you want to check a rut:

    Run.valid? '11.111.111-1'
      ====>>  true

    Run.valid? '100000000'
      ====>>  false

Example of valid formats are:

    11.111.111-1
    11111111-1
    111111111

    13.601.658-k
    13601658-k
    13601658k

    13.601.658-K
    13601658-K
    13601658K

If you need to generate a valid RUN:

    Run.generate


If you need to format a RUN:

    Run.format('111111111')
      ====>>  11.111.111-1

If you need to remove a RUN format:

    Run.remove_format('11.111.111-1')
      ====>>  111111111

## Assumptions

    You are saving the RUN as a string

## Notes

    RunCl uses [semantic versioning](http://semver.org/).

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
