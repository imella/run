require 'run_cl/version'
require 'run_cl/run'
require 'run_cl/run_validator'
require 'run_cl/uniq_run_validator'


module RunCl

  module ActAsRun
    extend ActiveSupport::Concern

    module InstanceMethods
    end

    module ClassMethods
      include InstanceMethods
      def has_run_cl(name, options = {})
        if not options[:run].presence or options[:run]
          if options[:on].presence
            validates name, run: true, on: options[:on]
          else
            validates name, run: true
          end
        end

        if not options[:uniq_run].presence or options[:uniq_run]
          if options[:on].presence
            validates name, uniq_run: true, on: options[:on]
          else
            validates name, uniq_run: true
          end
        end

        before_save "make_#{name}_format!"

        define_method "make_#{name}_format!" do
          self.send("#{name}=", Run.remove_format(self.send("#{name}")) )
        end
      end
    end
  end
end
