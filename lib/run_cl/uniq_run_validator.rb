# encoding: utf-8

class UniqRunValidator < ActiveModel::Validator
  def validate record
    options[:attributes].each do |attribute|
      run = Run.remove_format record.send(attribute)
      if record.new_record?
        record.errors[attribute.to_sym] << 'ya está en uso!!!' if record.class.where(attribute => run).any?
      else
        record.errors[attribute.to_sym] << 'ya está en uso!!!' if record.class.where('? = ? AND id != ?', attribute, run, record.id).any?
      end
    end
  end

  private
  # record.errors[attribute.to_sym] << I18n.t('run.in_use') unless Run.valid? record.send(attribute)
end